#!/bin/bash

cd '/usr/local/'

if [ ! -d "src" ]
  then
    mkdir "src"
    cd "/usr/local/src"
else
  cd "src"
  if [ -d "redis-stable" ]
    then
      rm -rf -- "redis-stable"
  fi
fi

rm -rf -- "/usr/local/redis"
rm -rf -- "/usr/local/redis-stable"

cd "/usr/local/src"
curl --remote-name "http://download.redis.io/releases/redis-stable.tar.gz"

tar -xzvf "redis-stable.tar.gz"
cd "redis-stable"

make
make PREFIX='/usr/local/redis-stable' install
ln -s 'redis-stable' '/usr/local/redis'

touch ~/.bash_profile
echo 'export PATH=/usr/local/redis/bin:$PATH' >> ~/.bash_profile
source ~/.bash_profile

mkdir -p '/usr/local/var/redis'

REDIS_SETTINGS="# Data folder dir\n/usr/local/var/redis\n# Bind to localhost\nbind 127.0.0.1"
echo "$REDIS_SETTINGS" > '/usr/local/redis/redis.conf'

alias redis_start='redis-server /usr/local/redis/redis.conf'

clear
echo "Redis has been installed"
